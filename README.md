# VERY SHORT DESCRIPTION OF FOLDER CONTENT:

## 1. DATABASE
The following two files contain the answer to the first part of the test, regarding SQL queries.

  + **database_answers.md**:
  + **database_answers.md.html**: HTML version of database_answer.md

## 2. DATA VALIDATION
The following three files are md, html and python script version of the second part of the test.

  + **data_validation.md**
  + **data_validation.md.html**
  + **data_validation.py** :
    + developed and tested for python 3.6.
    + It implements asynchronous request through asyncio and aiohttp.
    + A synchronous version is included in .md and .html files.
    + _Exec_: 'python data_validation.py -h' for help and parameters.
    + Defaul parameters should be fine if GBIF.db and script are in the same folder.
    + Output: print to stdout tsv rows.

## 3. PATTERN VALIDATION
The following four files contains the answers to the third part of the test.

  + **pattern_validation.R** : R script for outlier detection.
  + **pattern_validation.rmd**: R notebook.
    + Please download and run with Rstudio and knitr.
    + 'Rscript --quiet --vanilla pattern_validation.R --help' for help and execution.
    + 'Rscript --quiet --vanilla pattern_validation.R' should work if occurrence.csv is in the same folder.
    + Required libraries will be automatically installed if needed.
    + It takes a bit of time for rendering, please be patient and let me know if you found any issue.
  + **pattern_validation.html**: compiled html version of the R notebook. It will take a time to be ready.
  + **pattern_validation.pdf**: pdf version of R noteboo.
