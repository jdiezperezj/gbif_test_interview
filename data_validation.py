"""
This script takes a SQLite database containing geolocated animal observations,
and check if their countrycode data corresponds to the data stored in GBIF database.
It uses GBIF REST API through asynchronous requests.
"""

import sqlite3
import asyncio
import aiohttp


HEADERS = {
    'user-agent': ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) '
                   'AppleWebKit/537.36 (KHTML, like Gecko) '
                   'Chrome/45.0.2454.101 Safari/537.36'),
}


def get_records(db,
    query="SELECT distinct decimallatitude, decimallongitude,countrycode FROM occurrence",
    fields=['decimallatitude','decimallongitude','countrycode']):
    """
    Get database records for unique latitude, longitude and country code.
    :param db: str, database path.
    :return: generator, it yields a dict with specified parameters from database query.
    """
    with sqlite3.connect(db) as conn:
        cur = conn.cursor()
        cur.execute(query)
        for row in cur.fetchall():
            yield dict(zip(fields, row))


async def fetch(url, params, session):
    """
    Query function.
    :param url: str, api url.
    :param params: dict, url long and lat params.
    :param session: obj, session object.
    :return: tuple, REST API query and query arguments.
    """
    async with aiohttp.ClientSession() as session:
        async with session.get(url, headers=HEADERS) as resp:
            resp = await resp.json()
            return resp, params


async def bound_fetch(sem, url, params, session):
    """
    Wrapper function adding semaphore functionality.
    :param sem: int, number of concurrent task. Default 1000.
    :param url: str, api url.
    :param params: dict, url long and lat params.
    :param session: obj, session object.
    :return: tuple, returns fetch's function result.
    """
    async with sem:
        return await fetch(url, params, session)


async def run(r, db):
    """
    Main script's function.
    :param r: int, number of concurrent task. Default 1000.
    :param db: str, path to SQLite database.
    :return: void, prints to sys.stdout the result.
    """
    url = 'http://api.gbif.org/v1/geocode/reverse?lat={lat}&lng={lng}'
    tasks = []
    # create instance of Semaphore
    sem = asyncio.Semaphore(r)

    # Create client session that will ensure we dont open new connection
    # per each request.
    async with aiohttp.ClientSession() as session:
        for rec in get_records(db):
            # pass Semaphore and session to every GET request
            u = url.format(lat=rec['decimallatitude'],
                lng=rec['decimallongitude'])
            params = rec
            try:
                task = asyncio.ensure_future(bound_fetch(sem, u, params, session))
            except Exception as e:
                print(e)
            else:
                tasks.append(task)
        responses = asyncio.gather(*tasks)
        result = await responses
        print_all(result)


def print_all(responses):
    """
    :param reponses: list, values.
    :return: void, prints to sys.stdout the result.
    """
    for nn, r in enumerate(responses):
        p, k = r
        if len(p) > 1:
            for n, element in enumerate(p):
                element.update(k)
                print_element(element, ['ambiguous_country_codes', nn + 1, n + 1])
        elif p[0]['isoCountryCode2Digit'] != k['countrycode']:
            element = p[0]
            element.update(k)
            print_element(element, ['different_country_codes', nn + 1, 0])


def print_element(element, code):
    """
    :param element: list, values.
    :param code: list, reason to report.
    :return: void, prints to sys.stdout the result.
    """
    keys = ['subject'] + list(element.keys())
    values = code + list(element.values())
    print("\t".join(map(str, values)))


def get_parser():
    """
    Get parser object for script describe_geodatabase.py.
    :return: object, class argparse.ArgumentParser instance.
    """
    from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

    parser = ArgumentParser(description=__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument('-d', '--database', action='store', dest='database',
        required=False, type=str,
        default='/home/jdp/Documents/gbif_coding_test/GBIF/GBIF.db',
        help='Input database path.')

    parser.add_argument('-n', '--number', action='store', dest='number',
        required=False, type=int,
        default=1000,
        help='Number of concurrent tasks.')

    return parser


def main(args):
    """
    Main function.
    :param args: obj, argparse object.
    """
    number = args.number
    db = args.database
    loop = asyncio.get_event_loop()

    future = asyncio.ensure_future(run(number, db))
    loop.run_until_complete(future)


if __name__ == '__main__':
    args = get_parser().parse_args()
    main(args)
