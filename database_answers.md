# SQL TEST:
## 1.- A list of dataset.title, dataset.key of all datasets without occurrence records
```sql
SELECT d.key, d.title, 0 AS count FROM dataset AS d WHERE key NOT IN (SELECT datasetkey FROM occurrence) ORDER BY d.title DESC;
```
### Result:

+ 274a36be-0626-41c1-a757-3064e05811a4|VIS - Fishes in estuarine waters in Flanders, Belgium
+ dfddad59-5bc5-4e35-8b35-334eed43bba9|Freshwater samples in MZNA-INV-FRW: Macroinvertebrate samples from the water quality monitoring network along the Ebro Basin
+ e1f3be55-9f45-474c-8374-502b236e0ad0|Alien plant presence dataset from the point-radius plot surveys in 2010-2015 in Taiwan
+ 3acd6fb6-afce-49a3-a70d-070d4250229b|A second contender for “world’s smallest fly” (Diptera: Phoridae)

## 2.- A list of dataset.title, dataset.key and total count of occurrence records. It must display 0 for datasets without occurrence records

```sql
SELECT d.key, d.title, 0 as count FROM dataset AS d WHERE key NOT IN (SELECT datasetkey FROM occurrence)
UNION
SELECT d.key, d.title, COUNT(d.key) AS count FROM dataset AS d INNER JOIN occurrence AS o ON d.key = o.datasetkey GROUP BY d.key ORDER BY count DESC;
```
### Result:
+ dee8edc4-b19a-11e2-886d-00145eb45e9a|Floristic Databases of Mecklenburg-Pomerania - Higher Plants|7176
+ e6fab7b3-c733-40b9-8df3-2a03e49532c1|Flora von Deutschland (Phanerogamen)|4236
+ 6ac3f774-d9fb-4796-b3e9-92bf6c81c084|naturgucker|224
+ 4fa7b334-ce0d-4e88-aaae-2e0c138d049e|EOD - eBird Observation Dataset|41
+ 7bd65a7a-f762-11e1-a439-00145eb45e9a|Tropicos Specimen Data|38
+ 838e2626-f762-11e1-a439-00145eb45e9a|NODC WOD01 Plankton Database|31
+ 82a421d4-f762-11e1-a439-00145eb45e9a|Edaphobase|27
+ 292a71df-588b-48fa-9ab5-29ae868ba88c|Finnish Bird Ringing and Recovery Database|15
+ d5312bc2-819b-11e2-bad2-00145eb45e9a|Atlas der Raubfliegen Deutschlands - database|10
+ 71f84ac2-f762-11e1-a439-00145eb45e9a|MfN - Fossil plants (Paleophytic)|8
+ 821cc27a-e3bb-4bc5-ac34-89ada245069d|NMNH Extant Specimen Records|2
+ 197908d0-5565-11d8-b290-b8a03c50a862|Fishbase|1
+ 22331d58-4a7e-40da-b7db-652a4d4d85ed|HERBAM - Herbário da Amazônia Meridional|1
+ 4300f8d5-1ae5-49e5-a101-63894b005868|RB - Rio de Janeiro Botanical Garden Herbarium Collection|1
+ d82d5a18-0428-4e52-be16-f509153e8126|UB - Herbário da Universidade de Brasília|1
+ 274a36be-0626-41c1-a757-3064e05811a4|VIS - Fishes in estuarine waters in Flanders, Belgium|0
+ 3acd6fb6-afce-49a3-a70d-070d4250229b|A second contender for “world’s smallest fly” (Diptera: Phoridae)|0
+ dfddad59-5bc5-4e35-8b35-334eed43bba9|Freshwater samples in MZNA-INV-FRW: Macroinvertebrate samples from the water quality monitoring network along the Ebro Basin|0
e1f3be55-9f45-474c-8374-502b236e0ad0|Alien plant presence dataset from the point-radius plot surveys in 2010-2015 in Taiwan|0

## 3.- A list of dataset.title, dataset.key and total count of records, where the count of records is greater than 15

```sql
SELECT d.key, d.title, count(d.key) AS count FROM dataset AS d INNER JOIN occurrence AS o ON d.key = o.datasetkey GROUP BY d.key HAVING COUNT > 15  ORDER BY count DESC;
```
### Result:
+ dee8edc4-b19a-11e2-886d-00145eb45e9a|Floristic Databases of Mecklenburg-Pomerania - Higher Plants|7176
+ e6fab7b3-c733-40b9-8df3-2a03e49532c1|Flora von Deutschland (Phanerogamen)|4236
+ 6ac3f774-d9fb-4796-b3e9-92bf6c81c084|naturgucker|224
+ 4fa7b334-ce0d-4e88-aaae-2e0c138d049e|EOD - eBird Observation Dataset|41
+ 7bd65a7a-f762-11e1-a439-00145eb45e9a|Tropicos Specimen Data|38
+ 838e2626-f762-11e1-a439-00145eb45e9a|NODC WOD01 Plankton Database|31
+ 82a421d4-f762-11e1-a439-00145eb45e9a|Edaphobase|27

## 4.- An ordered list of dataset keys and total count of records, ordered descending by number or records

```sql
SELECT d.key, 0 AS COUNT FROM dataset AS d WHERE key NOT IN (SELECT datasetkey FROM occurrence)
UNION
SELECT d.key, count(d.key) AS count FROM dataset AS d INNER JOIN occurrence AS o ON d.key = o.datasetkey GROUP BY d.key ORDER BY count DESC;
```
### Result:
+ dee8edc4-b19a-11e2-886d-00145eb45e9a|7176
+ e6fab7b3-c733-40b9-8df3-2a03e49532c1|4236
+ 6ac3f774-d9fb-4796-b3e9-92bf6c81c084|224
+ 4fa7b334-ce0d-4e88-aaae-2e0c138d049e|41
+ 7bd65a7a-f762-11e1-a439-00145eb45e9a|38
+ 838e2626-f762-11e1-a439-00145eb45e9a|31
+ 82a421d4-f762-11e1-a439-00145eb45e9a|27
+ 292a71df-588b-48fa-9ab5-29ae868ba88c|15
+ d5312bc2-819b-11e2-bad2-00145eb45e9a|10
+ 71f84ac2-f762-11e1-a439-00145eb45e9a|8
+ 821cc27a-e3bb-4bc5-ac34-89ada245069d|2
+ 197908d0-5565-11d8-b290-b8a03c50a862|1
+ 22331d58-4a7e-40da-b7db-652a4d4d85ed|1
+ 4300f8d5-1ae5-49e5-a101-63894b005868|1
+ d82d5a18-0428-4e52-be16-f509153e8126|1
+ 274a36be-0626-41c1-a757-3064e05811a4|0
+ 3acd6fb6-afce-49a3-a70d-070d4250229b|0
+ dfddad59-5bc5-4e35-8b35-334eed43bba9|0
+ e1f3be55-9f45-474c-8374-502b236e0ad0|0

## 5.- What is the month of the year with most records reported?

```sql
SELECT moth FROM (SELECT strftime('%m', eventdate) AS month,  count(*) AS count FROM occurrence GROUP BY month ORDER BY count DESC LIMIT 1);
```
### Result:
+ 01

## 6.- What is the month of the year with least records reported?

```sql
SELECT month FROM (SELECT strftime('%m', eventdate) AS month,  count(*) AS count FROM occurrence GROUP BY month ORDER BY count ASC LIMIT 1);
```
### Result:
+ 02

## 7.- Final comment:
```sql
UPDATE occurrence SET countrycode = 'DE' WHERE countrycode = 'GE';
```
